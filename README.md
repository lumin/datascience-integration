# Datascience Software Integration

1. Stan-math (NEW) [pkg:ok][upl:/]
2. Stan (NEW) [pkg:testing][upl:/]
3. Rabit (NEW) [pkg:ok][upl:new]
4. Xgboost (NEW) [pkg:ok][upl:new][autopkgtest:WIP]
5. oneDNN (2.0~beta6 -> 2.0~beta7) [pkg:new]
6. onnx (1.6.0 -> 1.7.0) [pkg:ok][upl:ok]
7. nnpack [copyright:ok][upl:ok]
8. python-peachpy [bugfix:ok][upl:ok]
9. asmjit [upd:ok][upl:ok]
10. pytorch (1.5.0->1.5.0) [pkg:ok][upl:ok]
11. ideep [pkg:ok][upl:new]
12. psimd (version bump) [upl:ok]
13. pytorch-vision (0.6.0->0.6.1) [pkg:ok][upl:new]
14. pytorch-text (0.6.0) [pkg:ok][upl:new]
15. pytorch-audio (0.6.0) [pkg:ok][upl:new]
16. pytorch-ignite (0.4.1) [pkg:ok][upl:new]
17. tensorpipe (master) [pkg:ok][upl:new]
18. pytorch (1.6.0) [upd:ok][upl:ok]
19. skorch (0.9.0) [pkg:ok][upl:new]
20. cpuinfo (master) [upd:ok][upl:ok]
21. pthreadpool (master) [upd:ok][upl:ok]
22. xnnpack (master) [upd:ok][upl:ok]
23. fp16 (master) [upd:ok][upl:ok]
24. gloo (master) [upd:ok][upl:ok]
25. pytorch-vision (0.7.0 th=1.6.0) [upd:ok][upl:new]
26. pytorch-audio (th=1.6.0) [upd:ok][upl:ok]
27. onednn (2.0~beta8) [upd:ok][upl:new]
28. gemmlowp (master) (tensorflow deps) [upd:ok][upl:ok]
29. fbgemm (master) [upd:blocked-by-asmjit][upl:/]
30. opencv (4.4.0) [upd:ok][upl:new]
31. stan-math (3.3.0) [upd:ok][upl:/]
32. sleef [upd:ok][upl:ok]
33. dmlc-core (copyright fix) [upd:ok][upl:ok]
34. dmlc-core (master) [upd:ok][upl:ok]
35. rabit (master) [upd:ok][upl:/ok]
36. xgboost (1.2.0) [upd:ok][upl:ok]
37. highwayhash (master+arm64 fix) [upd:ok][upl:ok]
38. nsync (1.24.0) [upd:ok][upl:ok]


## Task List


    https://facebook.github.io/prophet/
    https://github.com/stan-dev/cmdstan
    https://github.com/stan-dev/cmdstanpy
    https://github.com/stan-dev/rstan
    https://github.com/stan-dev/pystan
